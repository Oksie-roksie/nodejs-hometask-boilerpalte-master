const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    getAllFighters() {
        const fighters = FighterRepository.getAll();
        if (!fighters.length) {
            return null;
        }
        return fighters
    }

    getFighterById(id) {
        const fighter = FighterRepository.getOne(id);
        if (!fighter) {
            return null;
        }
        return fighter
    }

    updateFighter(id, newData) {
        const fighter = FighterRepository.update(id, newData);
        if (!fighter.id) {
            return null;
        }
        return fighter
    }

    createFighter(data) {
        return FighterRepository.create(data);
    }

    deleteFighter(id) {
        const fighter = FighterRepository.delete(id);
        if (!fighter) {
            return null;
        }
        return fighter
    }
}

module.exports = new FighterService();