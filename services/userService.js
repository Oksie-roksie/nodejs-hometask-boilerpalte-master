const { UserRepository } = require('../repositories/userRepository');

class UserService {
    // TODO: Implement methods to work with user

    getAllUsers() {
        const users = UserRepository.getAll();
        if (!users.length) {
            return null;
        }
        return users
    }

    getUserById(id) {
        const user = UserRepository.getOne(id);
        if (!user) {
            return null;
        }
        return user
    }

    updateUser(id, newData) {
        const user = UserRepository.update(id, newData);
        if (!user.id) {
            return null;
        }
        return user
    }

    createUser(data) {
        return UserRepository.create(data);
    }

    deleteUser(id) {
        const user = UserRepository.delete(id);
        if (!user) {
            return null;
        }
        return user
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();